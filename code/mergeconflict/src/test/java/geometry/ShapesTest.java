package geometry;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * Unit test for simple App.
 */
public class ShapesTest {
   
    @Test
    public void shouldGiveAreaOfRectangle() {
        Rectangle rec = new Rectangle(2,3);
        assertEquals("Checking if the area is true", 6, rec.getArea());
    }

    @Test
    public void testingtoSring() {
        Rectangle rec = new Rectangle(2,3);
        assertEquals("Checking toString method", "Length: "+2+" , Width: "+3, rec.toString());
    }

    @Test
    public void testTriangleGetMethods() {
        Triangle t = new Triangle(4.0, 6.0);
        // assertEquals("Making sure get methods return proper values", 1.0, t.getBase(), 0.00000000000001);
        // assertEquals("Making sure get methods return proper values", 2.0, t.getHeight(), 0.00000000000001);
        assertEquals("Making sure get methods return proper values", 4.0, t.getBase(), 0.00000000000001);
        assertEquals("Making sure get methods return proper values", 6.0, t.getHeight(), 0.00000000000001);
    }

    @Test
    public void testTriangleGetArea() {
        Triangle t = new Triangle(4.0, 6.0);
        // assertEquals("Making sure getArea method returns proper value", 23.0, t.getArea(), 0.00000000000001);
        assertEquals("Making sure getArea method returns proper value", 12.0, t.getArea(), 0.00000000000001);
    }

    @Test
    public void testTriangleToString() {
        Triangle t = new Triangle(4.0, 6.0);
        // assertEquals("Making sure toString method returns proper value", "Base: 3.0 ; Height: 2.0", t.toString());
        assertEquals("Making sure toString method returns proper value", "Base: 4.0 ; Height: 6.0", t.toString());
    }
}
