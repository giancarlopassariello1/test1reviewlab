package geometry;

public class Shapes {
    public static void main(String[] args) {
        Rectangle rec = new Rectangle(2,4);
        System.out.println(rec);
        System.out.println(rec.getArea());
        
        Triangle test = new Triangle(4.0, 6.0);
        System.out.println(test);
        System.out.println(test.getArea());
    }
    
}
