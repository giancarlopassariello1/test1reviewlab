package geometry;

public class Triangle {
    private double base;
    private double height;

    public Triangle(double base, double height) {
        this.base = base;
        this.height = height;
    }

    public double getBase() {
        return this.base;
    }

    public double getHeight() {
        return this.height;
    }

    public double getArea() {
        return (this.height*this.base)/2;
    }

    public String toString() {
        return "Base: " + this.base + " ; Height: " + this.height;
    }
}