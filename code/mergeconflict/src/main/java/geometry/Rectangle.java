package geometry;

public class Rectangle {
    private int length;
    private int width;

    public Rectangle(int length, int width) {
        this.length = length;
        this.width = width;
    }

    public int getArea() {
        return this.length * this.width;
    }
    
    public String toString() {
        return "Length: "+this.length+" , Width: "+this.width;
    }
}
